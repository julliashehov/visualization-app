import numpy as np
from numpy import (array, transpose, atleast_1d, atleast_2d,
                   ravel, asarray)

from scipy.interpolate import _fitpack_py
from scipy.interpolate import dfitpack


class Interp2d:
    def __init__(self, x, y, z, kind='linear'):
        x = ravel(x)
        y = ravel(y)
        z = asarray(z)

        if z.ndim == 2:
            if z.shape != (len(y), len(x)):
                raise ValueError("When on a regular grid with x.size = m "
                                 "and y.size = n, if z.ndim == 2, then z "
                                 "must have shape (n, m)")
        if not np.all(x[1:] >= x[:-1]):
            j = np.argsort(x)
            x = x[j]
            z = z[:, j]
        if not np.all(y[1:] >= y[:-1]):
            j = np.argsort(y)
            y = y[j]
            z = z[j, :]
        z = ravel(z.T)

        interpolation_types = {'linear': 1, 'cubic': 3, 'quintic': 5}
        kx = ky = interpolation_types[kind]

        nx, tx, ny, ty, c, fp, ier = dfitpack.regrid_smth(
            x, y, z, None, None, None, None,
            kx=kx, ky=ky, s=0.0)
        self.tck = (tx[:nx], ty[:ny], c[:(nx - kx - 1) * (ny - ky - 1)],
                    kx, ky)

    def __call__(self, x, y):
        x = atleast_1d(x)
        y = atleast_1d(y)

        if x.ndim != 1 or y.ndim != 1:
            raise ValueError("x and y should both be 1-D arrays")

        x = np.sort(x, kind="mergesort")
        y = np.sort(y, kind="mergesort")

        z = _fitpack_py.bisplev(x, y, self.tck, 0, 0)
        z = atleast_2d(z)
        z = transpose(z)

        if len(z) == 1:
            z = z[0]
        return array(z)
