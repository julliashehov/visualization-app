def _basic_polynomial(x_values, y_values, xi, yj):
    def calculate(x, y):
        numerator = denominator = 1
        for j in range(len(y_values)):
            if j == yj: continue
            for i in range(len(x_values)):
                if i == xi: continue
                numerator *= (x - x_values[i]) * (y - y_values[j])
                denominator *= (x_values[xi] - x_values[i]) * (y_values[yj] - y_values[j])
        return numerator / denominator
    return calculate


def _lagrange_polynomial(x_values, y_values, f_values):
    basic_polynomials = []
    for yj in range(len(y_values)):
        inner_basic_polynomials = []
        for xi in range(len(x_values)):
            inner_basic_polynomials.append(_basic_polynomial(x_values, y_values, xi, yj))
        basic_polynomials.append(inner_basic_polynomials)

    def calculate(x, y):
        result = 0
        for yj in range(len(y_values)):
            for xi in range(len(x_values)):
                result += f_values[yj][xi] * basic_polynomials[yj][xi](x, y)
        return result
    return calculate


def interp_lagrange(x_values, y_values, f_values, kind=''):
    lagrange = _lagrange_polynomial(x_values, y_values, f_values)

    def interp_predict(x_data, y_data):
        result = []
        for y in y_data:
            inner_result = []
            for x in x_data:
                inner_result.append(lagrange(x, y))
            result.append(inner_result)
        return result
    return interp_predict


if __name__ == '__main__':

    test_lagrange = interp_lagrange(pos, ind, valuesT)
    print(*test_lagrange(pos, ind), sep="\n")
