import os
import sys
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QWidget, QMessageBox, QPushButton,
                             QApplication, QDesktopWidget, QHBoxLayout,
                             QVBoxLayout, QLabel, QRadioButton, QButtonGroup,
                             QComboBox, QFileDialog)
from PyQt5.QtGui import QPixmap, QCloseEvent

from lagrange import interp_lagrange
from linear import Interp2d
from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.units import cm
from reportlab.lib.utils import ImageReader



class Application(QWidget):

    def __init__(self):
        super().__init__()

        self.closeEvent = self.on_closing
        self.bth_file_choose = QPushButton('Select file', self)
        self.bth_file_choose.setMaximumSize(115, 30)
        self.bth_file_choose.clicked.connect(self.open_file_dialog)
        self.lbl_file_choose = QLabel('File NOT selected', self)

        self.lbl_mode_choose = QLabel('Mode', self)
        self.radio_button_model = QRadioButton('Modeling')
        self.radio_button_model.setChecked(True)
        self.radio_button_real = QRadioButton('Real data')
        self.radio_button_all = QRadioButton('General case')
        self.mode_button_group = QButtonGroup()
        self.mode_button_group.addButton(self.radio_button_model)
        self.mode_button_group.addButton(self.radio_button_real)
        self.mode_button_group.addButton(self.radio_button_all)
        self.mode_button_group.buttonClicked.connect(self.mode_radio_clicked)

        self.interp_type = {
            'Linear': (Interp2d, 'linear'),
            'Cubic': (Interp2d, 'cubic'),
            'Lagrange': (interp_lagrange, ''),
        }
        self.interp_func = self.interp_type.get('Linear')
        self.lbl_interp_choose = QLabel('Interpolation', self)
        self.cmb_interp_choose = QComboBox(self)
        self.cmb_interp_choose.addItems(self.interp_type.keys())
        self.cmb_interp_choose.activated[str].connect(self.interp_choose)

        self.bth_vis = QPushButton('Visualize', self)
        self.bth_vis.setMinimumSize(150, 50)
        self.bth_vis.clicked.connect(self.current_mode)

        self.lbl_cmap_choose = QLabel('Selecting a Colormap:', self)
        self.lbl_cmap_choose.setAlignment(Qt.AlignCenter)

        self.cmap_rainbow = QPixmap('1.png')
        self.lbl_rainbow_pix = QLabel()
        self.lbl_rainbow_pix.setPixmap(self.cmap_rainbow)
        self.cmap_magma = QPixmap('2.png')
        self.lbl_magma_pix = QLabel()
        self.lbl_magma_pix.setPixmap(self.cmap_magma)
        self.cmap_gray = QPixmap('3.png')
        self.lbl_gray_pix = QLabel()
        self.lbl_gray_pix.setPixmap(self.cmap_gray)

        self.radio_button_rainbow = QRadioButton('Rainbow')
        self.radio_button_rainbow.setChecked(True)
        self.radio_button_magma = QRadioButton('Magma')
        self.radio_button_gray = QRadioButton('Gray')
        self.cmap_button_group = QButtonGroup()
        self.cmap_button_group.addButton(self.radio_button_rainbow)
        self.cmap_button_group.addButton(self.radio_button_magma)
        self.cmap_button_group.addButton(self.radio_button_gray)
        self.cmap_button_group.buttonClicked.connect(self.cmap_radio_click)
        self.file_name = ''
        self.index_cmap = self.curr_mode = 0

        self.bth_save = QPushButton('Save as pdf', self)
        self.bth_save.setMinimumSize(150, 50)
        self.bth_save.clicked.connect(self.pdf_save)

        self.init_ui()

    def __file_check_decorator(func):
        def decorator(self):
            try:
                func(self)
            except Exception as e:
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Information)

                msg.setText("Error")
                msg.setInformativeText("Wrong mode selected")
                msg.setWindowTitle("Error")
                msg.exec_()
        return decorator

    def interp_choose(self, text):
        self.interp_func = self.interp_type.get(text)

    def init_ui(self):

        # размещение в макете кнопки выбора файла и метки с именем файла
        h_box_top_line = QHBoxLayout()
        h_box_file_choose = QHBoxLayout()
        v_box_file_choose = QVBoxLayout()
        h_box_file_choose.addLayout(v_box_file_choose)
        v_box_file_choose.addWidget(self.bth_file_choose)
        v_box_file_choose.addWidget(self.lbl_file_choose)
        v_box_file_choose.addStretch(1)
        h_box_file_choose.setSpacing(15)
        h_box_file_choose.setContentsMargins(15, 20, 30, 30)
        h_box_file_choose.setSpacing(30)
        # добавляем слева в верхнюю полосу виджетов макет с кнопкой и меткой выбора файла
        h_box_top_line.addLayout(h_box_file_choose)

        h_box_mode_choose = QHBoxLayout()
        h_box_mode_choose.setSpacing(30)
        h_box_mode_choose.setContentsMargins(15, 20, 30, 30)
        h_box_mode_choose.setAlignment(Qt.AlignLeft | Qt.AlignTop)
        h_box_mode_choose.addWidget(self.lbl_mode_choose)
        h_box_mode_choose.addWidget(self.radio_button_model)
        h_box_mode_choose.addWidget(self.radio_button_real)
        h_box_mode_choose.addWidget(self.radio_button_all)

        h_box_mode_choose.addWidget(self.lbl_interp_choose)
        h_box_mode_choose.addWidget(self.cmb_interp_choose)

        # добавляем в верхнюю полосу виджетов макет с выбором режима
        h_box_top_line.addLayout(h_box_mode_choose)

        h_box_top_line.addStretch(1)
        h_box_vis = QHBoxLayout()
        h_box_vis.setAlignment(Qt.AlignRight)
        h_box_vis.addWidget(self.bth_vis)
        h_box_top_line.addStretch(1)

        # добавляем в верхнюю полосу виджетов макет с кнопкой построить
        h_box_top_line.addLayout(h_box_vis)

        # добавляем в основной вертикальный макет верхнюю полосу виджетов
        v_box_window = QVBoxLayout()
        v_box_window.addLayout(h_box_top_line)

        # добавляем в основной вертикальный макет метку выбора цветовой карты
        v_box_window.addWidget(self.lbl_cmap_choose)

        # rainbow
        h_box_rainbow = QHBoxLayout()
        h_box_rainbow.addWidget(self.radio_button_rainbow)
        h_box_rainbow.addWidget(self.lbl_rainbow_pix)
        v_box_window.addLayout(h_box_rainbow)

        # magma
        h_box_magma = QHBoxLayout()
        h_box_magma.addWidget(self.radio_button_magma)
        h_box_magma.addWidget(self.lbl_magma_pix)
        v_box_window.addLayout(h_box_magma)

        # gray
        h_box_gray = QHBoxLayout()
        h_box_gray.addWidget(self.radio_button_gray)
        h_box_gray.addWidget(self.lbl_gray_pix)
        v_box_window.addLayout(h_box_gray)

        h_box_btn_save = QHBoxLayout()
        h_box_btn_save.addWidget(self.bth_save)
        h_box_btn_save.setContentsMargins(450, 40, 500, 0)
        v_box_window.addLayout(h_box_btn_save)

        v_box_window.setAlignment(Qt.AlignHCenter | Qt.AlignTop)

        self.setLayout(v_box_window)

        v_box_window.addStretch(1)

        self.resize(950, 550)
        self.center()

        self.setWindowTitle('Visualization of thermal fields')
        self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def open_file_dialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        f_name, _ = QFileDialog.getOpenFileName(self, "Select file", "",
                                                "CSV files (*.csv);; All Files (*)", options=options)
        if f_name:
            self.file_name = os.path.basename(f_name).strip()  # только имя файла с расширением
            self.lbl_file_choose.setText(self.file_name)

    def cmap_radio_click(self):
        self.index_cmap = [self.cmap_button_group.buttons()[x].isChecked()
                           for x in range(len(self.cmap_button_group.buttons()))].index(True)

    def mode_radio_clicked(self):
        self.curr_mode = [self.mode_button_group.buttons()[x].isChecked()
                          for x in range(len(self.mode_button_group.buttons()))].index(True)

    def current_mode(self):
        if self.radio_button_all.isChecked():
            self.visualization_all()
        else:
            self.visualization()


    # закрытие всех окон с графиками
    def close_all(self, count):
        i = 1
        while i <= count:  # цикл по всем открытым окнам с графиками
            if plt.fignum_exists(i):  # если окно открыто,
                plt.close(i)  # то закрываем его
            i += 1

    # событие закрытия главного окна программы
    def on_closing(self, _e: QCloseEvent):
        count = 2
        if self.curr_mode:
            count = 4
        self.close_all(count)  # перед закрытием приложения - закрываем все окна с графиками

    @__file_check_decorator
    def visualization(self):  # это функция, чтобы строить графики для режимов "Моделирование" и "Реальные данные"

        lst_cmap = ['rainbow', 'magma', 'gray']  # названия цветовых схем
        lst_title = ['IR - surface temperature values', 'RM - temperature values ​​in internal tissues']

        count = 2  # определяем количество графиков
        if self.curr_mode:
            count = 4

        count_rows_by_set = 7  # количество строк на одну матрицу
        skip_rows = 0  # количество пропускаемых строк
        if not self.file_name:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)

            msg.setText("Error")
            msg.setInformativeText("File NOT selected!")
            msg.setWindowTitle("Error")
            msg.exec_()
            return

        self.close_all(count)  # перед построением новых графиков - закрываем все окна с предыдущими графиками
        df = pd.read_csv(self.file_name)
        figures = []
        # цикл по количеству отображаемых окон
        for i in range(count):
            # создаём ТЕКУЩИЙ dataframe из части csv файла, то есть из очередной матрицы
            curr_df = df[skip_rows:skip_rows + count_rows_by_set]
            skip_rows += count_rows_by_set  # увеличиваем смещение на следующую матрицу
            # далее работаем с графиками ТОЛЬКО ТЕКУЩЕГО dataframe
            pos = np.array(curr_df['pos'] / 180 * np.pi)
            ind = np.array(curr_df.columns[1:], dtype=np.int)
            values = np.array(curr_df[ind.astype('str')])

            # функция интерполяции
            interp_func, kind = self.interp_func
            func = interp_func(pos, ind, values.T, kind=kind)

            # вычисляем r, theta для рисования
            theta = np.linspace(0, 2 * np.pi, 200)  # theta
            r = np.linspace(0, 90, 100)  # r
            # создаём двумерные данные для рисования
            fig = func(theta, r)
            theta, r = np.meshgrid(theta, r)

            # выводим результат
            figure = plt.figure("Окно " + str(i + 1), figsize=(5, 5))  # Заголовок ОКНА
            plt.suptitle(lst_title[i % 2])  # надпись В ОКНЕ

            ax = plt.subplot(projection='polar')
            # используем pcolormesh для размытых границ цвета
            res = ax.pcolormesh(theta, r, fig, cmap=lst_cmap[self.index_cmap], shading='auto')
            ax.set_rgrids([30, 60, 90])
            plt.grid(c='black')
            plt.plot()
            figures.append(figure)
            plt.show()


    @__file_check_decorator
    def visualization_all(self):  # это функция, чтобы строить графики для режима "Общий случай"

        lst_cmap = ['rainbow', 'magma', 'gray']  # названия цветовых схем
        lst_title = 'Visualization of thermal fields'

        count = 1  # определяем количество графиков

        if not self.file_name:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)

            msg.setText("Error")
            msg.setInformativeText("File NOT selected!")
            msg.setWindowTitle("Error")
            msg.exec_()
            return

        self.close_all(count)
        df = pd.read_csv(self.file_name)

        figures = []

        # создаём ТЕКУЩИЙ dataframe из части csv файла, то есть из очередной матрицы
        #curr_df = df[skip_rows:skip_rows + count_rows_by_set]
        curr_df = df[:]
        #skip_rows += count_rows_by_set  # увеличиваем смещение на следующую матрицу
        # далее работаем с графиками ТОЛЬКО ТЕКУЩЕГО dataframe
        value_x = np.array(curr_df['x'])
        value_y = np.array(curr_df['y'])
        value_t = np.array(curr_df['t'])

        func = {(value_x[i], value_y[i]): value_t[i] for i in range(len(value_t))}
        value_x = sorted(set(value_x))
        value_y = sorted(set(value_y))
        value_t = [[func[x, y] for x in value_x if (x, y) in func] for y in value_y]

        check_t = sum(len(inner) for inner in value_t)

        if check_t != len(value_x) * len(value_y):
            value_error = [f'{x=}, {y=}' for x in value_x for y in value_y if (x, y) not in func]
            value_error = '\n'.join(value_error)
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)

            msg.setText("Error")
            msg.setInformativeText(f"Incomplete data.\nNo data for {value_error}")
            msg.setWindowTitle("Error")
            msg.exec_()
            return

        # функция интерполяции
        interp_func, kind = self.interp_func
        if kind == 'cubic' and check_t < 16:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)

            msg.setText("Error")
            msg.setInformativeText(f"For cubic interpolation, you need "
                                   f"at least 16 values, you have {check_t}")
            msg.setWindowTitle("Error")
            msg.exec_()
            return

        func = interp_func(value_x, value_y, value_t, kind=kind)

        set_x = np.linspace(min(value_x), max(value_x), 200)
        set_y = np.linspace(min(value_y), max(value_y), 100)

        # создаём двумерные данные для рисования
        fig = func(set_x, set_y)
        set_x, set_y = np.meshgrid(set_x, set_y)

        # выводим результат
        figure = plt.figure(figsize=(5, 5))
        plt.suptitle(lst_title)

        ax = plt.subplot()
        # используем pcolormesh для размытых границ цвета
        res = ax.pcolormesh(set_x, set_y, fig, cmap=lst_cmap[self.index_cmap], shading='auto')
        plt.grid(c='black')
        plt.plot()
        figures.append(figure)
        plt.show()

    def pdf_save(self, figures):
        pdf = PdfPages("Visualization of thermal fields.pdf")
        for figure in figures:
            pdf.savefig(figure)
        pdf.close()

if __name__ == '__main__':
    app = QApplication(sys.argv)

    ex = Application()

    sys.exit(app.exec_())
